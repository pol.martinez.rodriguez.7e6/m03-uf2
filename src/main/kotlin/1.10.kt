import java.util.*

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Escriu un marc per un String
 */
fun createBox (name:String, char:String) {
    for (i in 1 .. name.length+4){
        print(char)
    }
    println()
    print(char)
    for (i in 1 .. name.length+2){
        print(" ")
    }
    print(char)
    println()
    print("$char $name $char")
    println()
    print(char)
    for (i in 1 .. name.length+2){
        print(" ")
    }
    print(char)
    println()
    for (i in 1 .. name.length+4){
        print(char)
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix algo i un numero:")
    val name = scanner.nextLine()
    val char = scanner.next()
    createBox(name, char)
}