import java.util.*
import kotlin.math.exp

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Nombres creixents
 */
fun numberAscendent (primerNumero:Int):Boolean {
    var digit = digitCounter(primerNumero)
    var counter = 0
    if (primerNumero%10 > (primerNumero%100)/10 ) {
        if (counter<digit-1) {
            counter +=1
            return numberAscendent(primerNumero % 10)
        }
        else return true
    }
    else return false
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix un numero:")
    val primerNumero = scanner.nextInt()
    println(numberAscendent(primerNumero))
}