import java.util.*
import kotlin.math.exp

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Escriu una creu
 */
fun creatACross (primerNumero:Int, char:String) {
    for (i in 0 until primerNumero){
        if (i != primerNumero/2) {
            for (j in 1 ..  primerNumero/2) print(" ")
            println(char)
        }
        else {
            for (j in 1 .. primerNumero) print(char)
            println()
        }
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix el tamanya de la creu i el caracter:")
    val primerNumero = scanner.nextInt()
    val char = scanner.next()
    creatACross(primerNumero, char)
}