import java.util.*

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Seqüència d’asteriscos
 */
// 2^n - 1
fun asteriscSequence (primerNumero:Int) {
    if (1 < primerNumero){
        asteriscSequence(primerNumero-1)
    }
    for (i in 1 .. primerNumero){
        print("*")
    }
    println()
    if (primerNumero>1){
        asteriscSequence(primerNumero-1)
    }
}
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix un numero:")
    val primerNumero = scanner.nextInt()
    asteriscSequence(primerNumero)
}
//