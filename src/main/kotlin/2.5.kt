import java.util.*
import kotlin.math.exp

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Reducció de digits
 */
fun reduirDigits (primerNumero:Int):Int {
    var primerNumero = primerNumero
    var result = primerNumero
    if (digitCounter(primerNumero) != 1) {
        result = 0
        for (i in 1 .. digitCounter(primerNumero)){
            result += primerNumero%10
            primerNumero/=10
        }
        if (digitCounter(result)>1) return reduirDigits(result)
        else return result
    }
    else return result
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix un numero:")
    val primerNumero = scanner.nextInt()
    println(reduirDigits(primerNumero))
}