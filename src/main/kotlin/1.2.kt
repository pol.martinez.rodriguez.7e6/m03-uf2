import java.util.*

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: String és igual
 */
fun checkEquals (primeraFrase: String, segundaFrase: String):Boolean {
    return primeraFrase == segundaFrase
}
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix 2 coses:")
    var primeraFrase = scanner.next()
    var segundaFrase = scanner.next()
    println(checkEquals(primeraFrase, segundaFrase))
}