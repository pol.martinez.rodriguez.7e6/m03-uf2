import java.util.*

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: D’String a MutableList<Char>
 */
fun toMuttableList (primeraFrase: String): MutableList<Char> {
    val Lista = mutableListOf<Char>()
    for (i in primeraFrase){
        Lista.add(i)
    }
    return Lista
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix algo i un numero:")
    var primeraFrase = scanner.nextLine()

    println(toMuttableList(primeraFrase))
}