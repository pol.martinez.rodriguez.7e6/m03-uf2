import java.util.*
import kotlin.math.exp

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Factorial
 */
fun factorial (primerNumero:Int):Int {
    if (primerNumero>1) return primerNumero * factorial(primerNumero-1)
    else return 1
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix un numero:")
    val primerNumero = scanner.nextInt()

    println(factorial(primerNumero))
}