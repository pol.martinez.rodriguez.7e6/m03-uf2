import java.util.*

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Torres de Hanoi
 */
fun torreHanoi (num:Int, ori:String, aux:String, des:String) {
    if (num == 1){
        println("$ori => $des")
    }
    else{
        torreHanoi(num-1, ori, des, aux)
        println("$ori => $des")
        torreHanoi(num-1,  aux, ori, des)
    }
}
fun main  ()  {
    val scanner = Scanner(System.`in`)
    println("Introdueix tamany de la torre:")
    var num =scanner.nextInt()
    torreHanoi(num, "A", "B", "C")
}
// 2 ^ discos - 1
/*
Se puede mover un solo disco a la vez.
Un disco de mayor tamaño no puede estar sobre un disco más pequeño.
Solo se puede desplazar el disco que esté más arriba en cada varilla.
De esto se puede deducir que según el número de discos que haya en la torre,
se podrá resolver el problema con un número de pasos mínimo que corresponde con la siguiente relación:
 */
/*
Cas base es quan només tinc un disc, llavors només he de moure el disc 1 de la original a al destí.
El cas recursiu es basa en portar primer tots els discos al auxiliar i de ahi ja portar los al de destí.
Segueix un patró de moviments de manera que si treies la part del if num == 1 fallaria.
https://docs.google.com/document/d/1TReUJXGGWJy  AwzV3otrMwutYblaDK1izwpkHgkLDFVI/edit?usp=sharing
 */