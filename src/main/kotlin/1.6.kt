import java.util.*

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Divideix un String
 */
fun toMuttableListSplit (primeraFrase: String, char: Char): List<String> {
    if (primeraFrase.last() == char) {
        return primeraFrase.split(char).dropLast(1)
    }
    else return primeraFrase.split(char)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix algo i un numero:")
    var primeraFrase = scanner.nextLine()
    var char = scanner.next().single()

    println(toMuttableListSplit(primeraFrase, char))
}