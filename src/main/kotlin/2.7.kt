import java.util.*

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Primers perfectes
 */
fun checkPrime (num:Int):Boolean {
    var form=false
    var validate=0
    for (i in 1..num) {
        if( num%i==0) validate+=1
        if (validate==2 && i != num) {
            form =true
            break
        }
    }
    return validate==2 && !form
}
fun primeOfAll (num:Int):Boolean {
    return if (digitCounter(num) == 1) checkPrime(num)
    else checkPrime(reduirDigits(num)) && primeOfAll(reduirDigits(num))
}
fun main  ()  {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    var num =scanner.nextInt()
    print(primeOfAll(num))
}