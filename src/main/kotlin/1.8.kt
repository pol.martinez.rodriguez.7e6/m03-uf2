import java.util.*
import kotlin.math.exp

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Eleva’l
 */
fun createLine (primerNumero:Int, segonNumero:Int, char:String): String {
    var sentence = ""
    for (i in 1 .. primerNumero)  {
        sentence += " "
    }
    for (i in 1 .. segonNumero)  {
        sentence += char
    }
    return sentence
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un un numero d'espais, de llargada de caracter i el caracter:")
    val primerNumero = scanner.nextInt()
    val segonNumero =scanner.nextInt()
    val char = scanner.next()
    println(createLine(primerNumero, segonNumero, char))
}