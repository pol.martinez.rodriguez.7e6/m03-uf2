import java.util.*
import kotlin.math.exp

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Factorial
 */
fun doubleFactorial (primerNumero:Int):Int {
    if (primerNumero>1) return primerNumero * doubleFactorial(primerNumero-2)
    else return 1
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix un numero:")
    val primerNumero = scanner.nextInt()
    println(doubleFactorial(primerNumero))
}