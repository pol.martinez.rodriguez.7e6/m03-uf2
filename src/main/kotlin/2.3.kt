import java.util.*
import kotlin.math.exp

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Nombre de digits
 */
fun digitCounter (primerNumero:Int):Int {
    if (primerNumero > 0) return 1 + digitCounter(primerNumero/10)
    else return 0
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix un numero:")
    val primerNumero = scanner.nextInt()
    println(digitCounter(primerNumero))
}