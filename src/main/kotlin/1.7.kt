import java.util.*
import kotlin.math.exp

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Eleva’l
 */
fun elevat (primerNumero:Int, exponent:Int): Int {
    var result = 1
    if (exponent == 0) return result
    else {
        for (i in 1.. exponent){
            result *= primerNumero
        }
        return result
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix un número i el seu exponent:")
    val primerNumero = scanner.nextInt()
    val exponent =scanner.nextInt()

    println(elevat(primerNumero, exponent))
}