import java.util.*

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Caràcter d’un String
 */
fun selectChar (primeraFrase: String, number: Int): String {
    if (number < primeraFrase.length) return primeraFrase[number].toString()
    else return "error"
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix algo i un numero:")
    var primeraFrase = scanner.next()
    var number = scanner.nextInt()
    println(selectChar(primeraFrase, number))
}