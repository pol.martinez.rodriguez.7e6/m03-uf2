import java.util.*

/*
* AUTHOR: Pol Martinez Rodriguez
* DATE: 2022/12/14
*TITLE: Subseqüència
 */
fun subSequence (primeraFrase: String, start: Int, end:Int): String {
    if (start > primeraFrase.length || end> primeraFrase.length) return "error"
    else{
        if (start<end) return primeraFrase[start] + subSequence(primeraFrase, start+1, end)
        else return ""
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introodueix algo i un numero:")
    var primeraFrase = scanner.nextLine()
    var start = scanner.nextInt()
    var end  = scanner.nextInt()
    println(subSequence(primeraFrase, start,end))
}